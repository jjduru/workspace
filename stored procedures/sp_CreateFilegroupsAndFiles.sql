use dba_test
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CreateFilegroupsAndFiles]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_CreateFilegroupsAndFiles]
GO

create procedure sp_CreateFilegroupsAndFiles
(
	@database sysname
	,@table_name varchar(4000)
)
as
begin
	set nocount on;
	declare @mdf_location nvarchar(1000)
			,@filegroup nvarchar(1000)
			,@name1 nvarchar(1000)
			,@size nvarchar(200)
			,@filegrowth nvarchar(200)
			,@sqlFilegroup nvarchar(2000)
			,@sqlFilename1 nvarchar(4000)

	if (len(@database)=0) or @database is null
	begin
		set @database=db_name()
	end

	set @mdf_location=(
		select left(physical_name,LEN(physical_name)-CHARINDEX('\',REVERSE(physical_name))+1) 
		from sys.master_files mf   
		inner join sys.[databases] d
			on mf.[database_id] = d.[database_id]
		where d.[name] = ''+@database+'' AND mf.file_id=1
	)

	set @filegroup = @table_name
	set @name1 = @filegroup+'_1'

	set @size = '3072KB'
	set @filegrowth = '10240kb'

	set @sqlFilegroup ='
	use '+@database+';
	set nocount on;
	if not exists
	(
		select name from sys.filegroups where name = N'''+@filegroup+'''
	)
	begin
		alter database '+@database+'
			add filegroup ['+@filegroup+']
	end;'

	set @sqlFilename1='
	use '+@database+';
	set nocount on;
	declare @i1 int;
	exec master..xp_fileexist N'''+@mdf_location+@name1+'.ndf'', @i1 out
	if (@i1=''0'')
	begin
		alter database '+@database+'
			add file(
				name = N'''+@name1+''',
				filename = N'''+@mdf_location+@name1+'.ndf'',
				size = '+@size+',
				filegrowth = '+@filegrowth+'
			)
			to filegroup ['+@filegroup+']
		;
	end'


	--select @sqlFilegroup
	--	   +@sqlFilename1
	exec sp_executesql @sqlFilegroup
	exec sp_executesql @sqlFilename1
end
go